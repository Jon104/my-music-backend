import { mongoose } from '../common/mongoose';

const MusicSchema = mongoose.Schema({
	createdDate: {
		type: Number,
		required: [true, 'createdDate is required'],
	},
	name: {
		type: String,
		maxlength: [100, 'Name is too long (max 100 characters)'],
	},
	description: {
		type: String,
		maxlength: [500, 'Description is too long (max 500 characters)'],
	},
	url: {
		type: String,
		required: [true, 'Url is required'],
		maxlength: [255, 'Url is too long (max 255 characters)'],
	},
	userId: {
		type: String,
		required: [true, 'userId is required'],
	},
}, {collection: 'Music'});

MusicSchema.index({ description: "text" });
let MusicModel = mongoose.model("Music", MusicSchema);

export { MusicModel };
