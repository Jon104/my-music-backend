import { frontend_url } from "../constants";
import { UserModel } from "../models/user";
import { deleteMusic, errorMessage, UploadServices } from "../services";

const oauth = (req, res) => {
  res.redirect(`${frontend_url}/#/login`);
};

const oauthRedirect = (req, res) => {
  res.redirect(
    `${frontend_url}/#/?accessToken=${req.user.accessToken}&userId=${req.user.userId}`
  );
};

const readAll = (req, res) => {
  const limit = parseInt(req.query.perPage) || 10;
  const offset = parseInt(req.query.page) * limit;
  UserModel.countDocuments({})
    .then(
      count => {
        const totalEntries = count;
        const totalPages =
          totalEntries == 0 ? 0 : parseInt((totalEntries - 1) / limit) + 1;
        UserModel.find({})
          .limit(limit)
          .skip(offset)
          .then(
            rawData => {
              const data = {};
              data.totalEntries = totalEntries;
              data.totalPages = totalPages;
              data.items = rawData;
              res.json(data);
            },
            function(err) {
              errorMessage(res, 500, "Internal server error");
            }
          )
          .catch(function(err) {
            errorMessage(res, 500, "Internal server error");
          });
      },
      function(err) {
        errorMessage(res, 500, "Internal server error");
      }
    )
    .catch(function(err) {
      errorMessage(res, 500, "Internal server error");
    });
};

export { readAll, oauth, oauthRedirect };
