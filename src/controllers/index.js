import * as Home from "./home";
import * as Music from "./music";
import passport from "../common/OAuth";
import * as Users from "./users";
import frontend_url from "../constants";

import multer from "multer";
const upload = multer();

const controllers = app => {
  app.get("/", Home.home);
  app.get(
    "/auth/google",
    passport.authenticate("google", { scope: ["profile", "email"] }),
    Users.oauth
  );
  app.get(
    "/auth/google/redirect",
    passport.authenticate("google", {
      failureRedirect: `${frontend_url}/#/login`
    }),
    Users.oauthRedirect
  );
  app.get("/music/:userId", Music.readAllOfUser);
  app.get("/users", Users.readAll);
  app.post("/users/:userId/music", upload.any(), Music.create);
  app.delete("/users/:userId/music/:songId", Music.deleteOne);
};

export default controllers;
