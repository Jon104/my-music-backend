import { MusicModel } from "../models/music";
import { UserModel } from "../models/user";
import { deleteMusic, errorMessage, UploadServices } from "../services";

const create = (req, res) => {
  const music = new MusicModel(req.body);
  music.createdDate = Date.now();
  music.userId = req.params.userId;
  const fileName = music._id + req.files[0].originalname;
  const file = req.files[0].buffer;

  music.url = process.env.BUCKET_IMAGE_LINK + fileName;
  music.name = req.files[0].originalname;

  music.save().then(function(data) {
    UploadServices.uploadSample(fileName, file)
      .then(function(data) {
        res.status(201).json({ file: music });
      })
      .catch(function(err) {
        errorMessage(res, 500, "Internal server error");
      });
  });
};

const readAllOfUser = (req, res) => {
  const limit = parseInt(req.query.perPage) || 10;
  const offset = parseInt(req.query.page) * limit;

  UserModel.findOne({ id: req.params.userId })
    .then(
      function(data) {},
      function(err) {
        errorMessage(res, 400, "Missing parameter or unexisting user");
      }
    )
    .catch(function(err) {
      errorMessage(res, 500, "Internal server error");
    });

  MusicModel.countDocuments({ userId: req.params.userId })
    .then(
      function(count) {
        const totalEntries = count;
        const totalPages =
          totalEntries == 0 ? 0 : parseInt((totalEntries - 1) / limit) + 1;

        MusicModel.find({ userId: req.params.userId })
          .sort({ createdDate: 1 })
          .limit(limit)
          .skip(offset)
          .then(
            function(rawData) {
              const data = {};
              data.totalEntries = totalEntries;
              data.totalPages = totalPages;
              data.items = rawData;
              res.json(data);
            },
            function(err) {
              errorMessage(res, 500, "Internal server error");
            }
          )
          .catch(function(err) {
            errorMessage(res, 500, "Internal server error");
          });
      },
      function(err) {
        errorMessage(res, 500, "Internal server error");
      }
    )
    .catch(function(err) {
      errorMessage(res, 500, "Internal server error");
    });
};

const deleteOne = (req, res) => {
  MusicModel.findOne({ _id: req.params.songId, userId: req.params.userId })
    .then(function(data) {
      deleteMusic(data._id.toString().concat(data.name), res);

      MusicModel.deleteOne({
        _id: req.params.songId,
        userId: req.params.userId
      })
        .then(
          function(data) {
            if (data.n == 0) {
              errorMessage(
                res,
                400,
                "Missing parameter or unexisting picture for user"
              );
            }
            res.status(204).send("No Content");
          },
          function(err) {
            errorMessage(
              res,
              400,
              "Missing parameter or unexisting picture for user"
            );
          }
        )
        .catch(function(err) {
          errorMessage(res, 500, "Internal server error");
        });
    })
    .catch(function(err) {
      errorMessage(res, 500, "Internal server error");
    });
};

export { create, deleteOne, readAllOfUser };
